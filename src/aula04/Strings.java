package aula04;

public class Strings {

	/**
	 * @param args
	 *            Strings em Java
	 */

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// Formas de inicializar strings

		String textoPeba = new String("Texto"); // Não recomendado
		String textoMassa = "Texto"; // Recomendado
		String semTexto = ""; // String vazia

		// Manipulação de strings
		// Obtendo uma substring:

		String texto = " Isso é uma string ";
		System.out.println(texto.substring(4)); // Posição inicial
		System.out.println(texto.substring(0, 4)); // Intervalo entre a posição
													// inicial e final da string

		// Retorna o tamanho da string:
		System.out.println(texto.length());

		// Retorna um caractere na posição definida:
		System.out.println(texto.charAt(2));

		// Retorna o índice da primeira ocorrência do caractere definido:
		System.out.println(texto.indexOf('o'));

		// RETORNA O TEXTO EM CAIXA ALTA:
		System.out.println(texto.toUpperCase());

		// retorna o texto em caixa baixa:
		System.out.println(texto.toLowerCase());

		// Retira os espaços à esquerda e à direita da string:
		System.out.println(texto.trim());

		// Substitui um texto por outro, no todo ou em parte, conforme definido
		// nos parâmetros:
		System.out.println(texto.replace("Isso", "Aquilo"));

		// Concatenando strings:
		String nome = "Giancarlo ";
		String sobrenome = "Lima da Silva";

		System.out.println(nome + sobrenome);

	}

}
