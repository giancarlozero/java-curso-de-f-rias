package aula04;

public class VetoresMatrizes {

	/**
	 * @param args
	 *            Vetores e Matrizes
	 */

	public static void main(String[] args) {
		// Declarando um vetor de inteiros
		int[] vetor;

		// Alocando memória para 10 valores inteiros
		vetor = new int[10];

		// Definindo valores para cada um dos elementos do vetor
		vetor[0] = 10;
		vetor[1] = 20;
		vetor[2] = 30;
		vetor[3] = 40;
		vetor[4] = 50;
		vetor[5] = 60;
		vetor[6] = 70;
		vetor[7] = 80;
		vetor[8] = 90;
		vetor[9] = 100;

		/*
		 * Em um vetor, as posições são definidas a partir do número zero. Neste
		 * exemplo, a primeira posição é 0 e a última é 9.
		 */

		// Acessando um determinado valor no vetor
		System.out.println(vetor[3]);

		// Percorrendo e exibindo os valores do vetor
		for (int i = 0; i < 10; i += 1) {
			System.out.println("Valor da posição " + i + ": " + vetor[i]);
		}

		// Criando, inicializando e definindo valores de um vetor com um só
		// comando:
		int[] vetor2 = { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30 };

		// Acessando um determinado valor no vetor
		System.out.println(vetor2[5]);

		// Percorrendo e exibindo os valores do vetor
		for (int i = 0; i < 10; i += 1) {
			System.out.println("Valor da posição " + i + ": " + vetor2[i]);
		}

		// Matriz: vetor dentro de vetor
		// Declarando uma matriz de 3 linhas x 3 colunas
		int[][] matriz = new int[3][3];

		// Preenchendo uma matriz com alguns valores e o exibindo
		for (int y = 0; y < matriz.length; y++) {
			for (int x = 0; x < matriz.length; x++) {
				matriz[y][x] = y + x;
				System.out.print(matriz[y][x]);
			}
			System.out.println();

		}

	}

}
