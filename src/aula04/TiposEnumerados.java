package aula04;

public class TiposEnumerados {

	/**
	 * @param args
	 * Tipos enumerados em Java
	 */

	// Declarando um tipo enumerado com 3 estados
	enum SituacaoNota {
		APROVADO, REPROVADO, RECUPERACAO
	};

	public static void main(String[] args) {

		// Criando uma variável e instanciando o tipo enumerado
		SituacaoNota poo = SituacaoNota.APROVADO;

		// Exemplo
		int nota = 8;

		if (nota <= 3)
			poo = SituacaoNota.REPROVADO;
		else if (nota <= 6)
			poo = SituacaoNota.RECUPERACAO;
		else
			poo = SituacaoNota.APROVADO;

		System.out.println(poo);

	}

}
