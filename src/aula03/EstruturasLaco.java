package aula03;

public class EstruturasLaco {
	
	/**
	 * @param args
	 *            Estruturas de laço
	 */

	public static void main(String[] args) {

		// Estrutura de laço For - Para
		// for (declaração; condição; incremento)
		for (int i = 0; i < 100; i += 1) {
			// Bloco de código
		}
		
		// Estruturas de laço While e Do While
		int j = 0;
		while (j < 100) {
			// Bloco de código
		}
		
		int k = 0;
		do {
			// Bloco de código
		} while (k < 100);

	}

}
