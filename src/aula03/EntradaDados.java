package aula03;

import java.util.Scanner; // Biblioteca Scanner, de entrada de dados

	/**
	 * @param args
	 *            Entrada de dados com variável do tipo Scanner
	 */

public class EntradaDados {

	public static void main(String[] args) {
		System.out.println("Recebendo uma string: ");
		Scanner entrada = new Scanner(System.in); // Entrada de dados
		String texto = entrada.nextLine();
		entrada.close();
		// Convertendo o texto digitado para inteiro:
		// int numero = Integer.parseInt(texto);
		System.out.println(texto);

	}

}
