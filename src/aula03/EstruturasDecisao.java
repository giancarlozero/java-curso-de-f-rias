package aula03;

public class EstruturasDecisao {
	
	/**
	 * @param args
	 *            Estruturas de decisão
	 */
	
	public static void main(String[] args) {
		
		// Estrutura de decisão If/Else - Se/Senão
		int numero = 2;
		if (numero % 2 == 0) {
			// Bloco de código
		} else  {
			// Bloco de código
		}
		
		// Estrutura de decisão Switch/Case. No Java 7, também recebe uma string como parâmetro		
		switch (numero) {
		case 1:
			// Bloco de código
			break;
		case 2:
			// Bloco de código
			break;
		default:
			// Bloco de código
			break;
		}
	
	}

}
