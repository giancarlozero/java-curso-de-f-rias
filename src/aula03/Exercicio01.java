package aula03;

import java.util.Scanner;

public class Exercicio01 {

	/**
	 * @param args
	 *            EXERCÍCIO 01 - Escreva um algoritmo que receba 3 valores: o
	 *            múltiplo de um número, o início e o final de um intervalo de
	 *            números. O usuário terá que digitar cada um dos valores. Se o
	 *            início for maior do que o final do intervalo, isso deverá ser
	 *            tratado (não deve dar erro). O algoritmo deverá listar, com o
	 *            uso do PRINTF, todos os números do intervalo e dizer se cada
	 *            um deles é ou não é múltiplo do número escolhido.
	 */

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite um número múltiplo: ");
		int multiplo = entrada.nextInt();

		System.out.println("Digite o início do intervalo: ");
		int inicio = entrada.nextInt();

		System.out.println("Digite o final do intervalo: ");
		int fim = entrada.nextInt();
		entrada.close();

		if (inicio > fim) {
			System.out
					.println("Início do intervalo é maior do que o final. Invertendo valores.");
			int temp = 0;

			temp = inicio;
			inicio = fim;
			fim = temp;
		}

		for (int i = inicio; i <= fim; i += 1) {
			if (i % multiplo == 0) {
				System.out.printf("O número %s é múltiplo de %s \n", i,
						multiplo);
			} else {
				System.out.printf("O número %s não é múltiplo de %s \n", i,
						multiplo);
			}
		}
	}
}
