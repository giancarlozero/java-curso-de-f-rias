package aula02;

public class Operadores {

	/**
	 * @param args
	 *            Operadores em Java
	 *            http://docs.oracle.com/javase/tutorial/java/nutsandbolts/op1.html
	 */

	public static void main(String[] args) {

		// Pequeno exercício com operadores

		int numero = 0;

		numero = numero + 5;
		numero += 2;
		numero = numero - 2;
		numero = numero * 2;
		numero = numero / 2;

		System.out.println(numero);

	}

}
