package aula02;

public class IncrementoDecremento {
	
	/**
	 * @param args
	 *            Incremento e decremento de valores
	 */

	public static void main(String[] args) {

		int a = 0;

		System.out.println(a);

		// Incrementa após fazer qualquer outra ação
		System.out.println(a++);
		
		// Incrementa antes de fazer qualquer outra ação
		System.out.println(++a);
		
		// Decrementa após fazer qualquer outra ação
		System.out.println(a--);
		
		// Decrementa antes de fazer qualquer outra ação
		System.out.println(--a);
		
		// O mesmo que a = a + 1
		System.out.println(a += 1);
		
		// O mesmo que a = a - 1
		System.out.println(a -= 1);

	}

}
