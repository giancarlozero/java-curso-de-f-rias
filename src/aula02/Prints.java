
package aula02;

public class Prints {
	
	/**
	 * @param args
	 *            Tipos de comandos print em Java
	 */

	public static void main(String[] args) {

		// System.out.print(false); Imprime
		// System.out.println(); Imprime e pula uma linha
		// System.out.printf(null, null, args); Define um modelo e imprime

		// Exemplo printf

		int dia = 26;
		int mes = 3;
		int ano = 1984;

		System.out.printf("%s/%02d/%s", dia, mes, ano);
		
	}

}
