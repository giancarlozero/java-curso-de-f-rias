package aula05;

public class Funcoes {

	/**
	 * @param args
	 *            Funções em Java
	 */
	
	// Exemplo de função que calcula a fórmula de Bhaskara:
	
	public static double[] bhaskara(int a, int b, int c) {
	// Parâmetros 'a', 'b' e 'c', que receberão os valores digitados pelo usuário		
		int delta = (int) (Math.pow(b, 2) - (4 * a * c));
		
		double bhaskaraMais = (- b + Math.sqrt(delta)/2*a);
		double bhaskaraMenos = (- b - Math.sqrt(delta)/2*a);
		
		double[] resultado = {bhaskaraMais, bhaskaraMenos};
		
		return resultado; // Valor de retorno. Palavra-chave 'return', seguida da variável que se deseja mostrar o valor.
	}
	
	public static void main(String[] args) {
		
	}

}
